## 今日的学习计划
按照昨天制定的 TODO，列出以下两项：
- [ ] Git 获取远程仓库的所有操作和返回信息的收集
- [ ] Python 执行外部命令的的方式

## 过程记录
### git log
    - git log 无参数的替代情况下，git log会按时间先后顺序列出所有的提交，最近的更新排在最上面。列出每个提交的SHA-1校正和，作者的名字和电子邮件地址，提交时间以及提交说明。
    - git log -p 显示差异
    - git log --state 简略统计信息
    -- git log git log --pretty=oneline   显示一行，还有 full、fuller、short 等格式，看上去也差不错
    - git log --pretty=format定制显示的格式内容，比如git log --pretty=format:"%h - %an, %ar : %s" 。其他选项可参考 ref 里的链接。
    > @ZoomQuiet 在 ch1 任务里提到的监控各种活动数据，是否这里的内容就已经足够了？应该看不到其他分支的活动吧？
    - git log --since=2.weeks 两周内的日志
     > 作者给的一个实际的例子，如果要查看2008年10月期间Git源代码仓库中，由Junio Hamano提交的修改了测试文件并且尚未合并的提交，可以使用下面的命令：
```


$ git log --pretty="%h - %s" --author=gitster --since="2008-10-01" \
   --before="2008-11-01" --no-merges -- t/
5610e3b - Fix testcase failure when extended attributes are in use
acd3b9e - Enhance hold_lock_file_for_{update,append}() API
f563754 - demonstrate breakage of detached checkout with symbolic link HEAD
d1a43f2 - reset --hard/read-tree --reset -u: remove unmerged new paths
51a94af - Fix "checkout --track -b newbranch" on detached HEAD
b0ad11e - pull: allow "git pull origin $something:$current_branch" into an unborn branch
```

### git remote
-  git remote show origin 查看所有远程仓库
- git push origin dev 可创建远程分支
> 我们此次个人的分支就是这样被创建的吗？
- git remote show origin dev ,提示错误：
> 分支我在本地创建了，也 push 到远程了，为什么提示这个？
```
fatal: 'dev' does not appear to be a git repository
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.    
```

### git fetch
```
$ git fetch
remote: Enumerating objects: 58, done.
remote: Counting objects: 100% (58/58), done.
remote: Compressing objects: 100% (47/47), done.
remote: Total 58 (delta 9), reused 55 (delta 9), pack-reused 0
Unpacking objects: 100% (58/58), done.
From gitlab.com:101camp/5py/tasks
   efb6b39..42b7152  bruceq619   -> origin/bruceq619
   19ab121..aff009a  ddgwang     -> origin/ddgwang
   c145539..625fd5c  heyi        -> origin/heyi
   cee541f..ffa1303  ichme       -> origin/ichme
   840f883..e50348d  master      -> origin/master
   8bc4edd..54d306d  shankai2018 -> origin/shankai2018
   48cd264..64e78d9  wonius      -> origin/wonius
   ```
- 什么是内容寻址文件系统层

- .git 目录
    + 当执行 git init 会生成 .git 目录
    + 如若想备份或复制一个版本库，只需把这个目录拷贝至另一处即可
    + 目录结构如下，其中 :
        * description gitWeb 使用，这里无需关心：
        * config 包含项目配置项
        * info 目录包含一个全局性排除（global exclude）文件

```
HEAD
config*
description
hooks/
info/
objects/
refs/
```    

## 找到一个小工具，可参考
17 年发布的shell 脚本，叫git-dude 。其基本原理是用 git fetch查看是否有输出，然后分析 git log 内容，大致如此，没安装。作者原话：
> How it works
> It simply uses git fetch and parses its output to see what has changed. Then it formats new commit messages with git log and shows desktop notification with notify-send / kdialog (Linux) or growlnotify (OSX). All of this in infinite loop.

## 今日总结
### 完成项
- [x] 关于 git log 的操作参数非常多，可以定制任意内容来输出。
- [x] 掌握部分 git remote 的操作，但如何获取远程分支信息不清楚
- [ ]  Python外部命令执行，今晚再学习 


## ref
- Git book https://git-scm.com/book/zh/v2/Git-%E5%86%85%E9%83%A8%E5%8E%9F%E7%90%86-%E5%BA%95%E5%B1%82%E5%9 1%BD%E4%BB%A4%E5%92%8C%E9%AB%98%E5%B1%82%E5%91%BD%E4%BB%A4
- Git-dude https://github.com/sickill/git-dude 