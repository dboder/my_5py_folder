import numpy as np
x = np.array([1,2,3,4])
print(x.shape)
np.save('my_array',x)

#内置函数创建 ndarray
x = np.zeros((3,4),dtype=int)
x = np.ones((4,5))
x = np.full((4,3),5)
x = np.eye(5)
x = np.diag([10,20,30,50])
x = np.arange(10)